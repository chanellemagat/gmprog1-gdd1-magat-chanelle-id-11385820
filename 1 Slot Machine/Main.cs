﻿using System;

public class Example
{
    public static void Main()
    {
        //srand(time(0));
        float chip = 1000;
        var result = chip;

        Console.WriteLine("Player's Chips: $ {0}", chip); // $1000 
        Console.WriteLine("1. Play slot. 2. Exit.");
        string input = Console.ReadLine();
        int number;
        Int32.TryParse(input, out number);
        Console.WriteLine("");
        Console.WriteLine("Enter Bet:");
        string bet = Console.ReadLine();
        Console.WriteLine("You've entered: $ {0}", bet);
        Console.WriteLine("");

        Random rnd = new Random();
        Console.WriteLine(rnd.Next(2, 7));
        Console.WriteLine(rnd.Next(2, 7));
        Console.WriteLine(rnd.Next(2, 7));
        Console.ReadKey();
    }
}