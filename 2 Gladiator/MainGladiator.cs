﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Gladiator
{
    class Program
    {
        static void Main(string[] args)
        {
            bool isUserWrong = true;

            Console.WriteLine("Enter Name: ");
            string playerName = Console.ReadLine();
            while (isUserWrong)
            {
                Console.WriteLine("\nChoose type of warrior: ");
                string[] warriors = { "Archer", "Knight", "Wizard" };
                for (int i = 0; i < warriors.Length; i++)
                {
                    Console.WriteLine(warriors[i]);
                }
                string warriorChosen = Console.ReadLine();

                if (warriorChosen == "Archer")
                {
                    Player p1 = new Player("Archer", 99, 0);
                    p1.PrintStats();
                    isUserWrong = false;

                }
                else if (warriorChosen == "Knight")
                {
                    Player p2 = new Player("Knight", 100, 50);
                    p2.PrintStats();
                    isUserWrong = false;
                }
                else if (warriorChosen == "Wizard")
                {
                    Player p3 = new Player("Wizard", 100, 50);
                    p3.PrintStats();
                    isUserWrong = false;
                }
                else
                {
                    Console.WriteLine("You have entered an invalid input\nPress Enter and type again:\n");
                    Console.ReadLine();
                }
            }
            
            Console.ReadKey();
        }
        
       
    }
}
