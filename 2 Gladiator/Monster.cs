﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Gladiator
{
    class Monster
    {
        public string name;
        public int hp;
        public int attackDamage;

        public static Random r = new Random();

        public Monster(string name, int hp, int ad)
        {
            mName = name;
            HP = hp;
            Ad = ad;
        }

        public void Attack(Monster other)
        {
            int actualDamage = r.Next(attackDamage + 1);
            Console.Write(mName + " (" + hp + ") hits you " (" + other.hp + ") ");

        }
    }
}
