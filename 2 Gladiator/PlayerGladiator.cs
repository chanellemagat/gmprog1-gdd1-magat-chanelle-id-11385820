﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Gladiator
{
    
    class Player
    {

        public Player(string name, int hp, int sp, int lowDamage, int highDamage)
        {
            Name = name;
            HP = hp;
            SP = sp;
        }


        public void PrintStats()
        {
            Console.WriteLine("Name: " + Name);
            Console.WriteLine("Hp: " + HP.ToString());
            Console.WriteLine("Exp: " + SP.ToString());
        }

   
        public int HP {get; set;}
        public float SP
        {
            get;
            set;
        }
        public string Name;

        public void TakeDamage(int damage)
        {
            if ((HP - damage) < 0)
            {
                HP = 0;
            }
            else
            {
                HP -= damage;
            }
        }

        public bool IsDead()
        {
            bool result = (HP <= 0);
            return result;
        }
    }

}
