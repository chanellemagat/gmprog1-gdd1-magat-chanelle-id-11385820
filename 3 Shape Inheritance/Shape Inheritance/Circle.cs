﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Shape_Inheritance
{
    class Circle : Shape
    {


        public Circle()
        {
            float r = 0;
            radius = r;
        }

        private float radius;
        public float Radius {
            get
            {
                return radius;
            }
            set
            {
                if (value > 0)
                    radius = value;
            }
        }

        public override double getArea()
        {
            return Math.PI * radius * radius;
        }

        public void setRadius(int r)
        {
            radius = r; 
 
        }
        public Circle(int r)
        {
            radius = r;
        }


    }
}
