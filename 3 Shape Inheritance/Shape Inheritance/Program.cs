﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Shape_Inheritance
{
    class Program
    {
         static void Main(string[] args)
        {
             Shape Rect = new Rectangle(5,7);
             Shape Circle = new Circle(6);
             Shape Tria = new Triangle(6,8);
             Shape Square = new Square(7);



             /*Rect.setWidth(5);
             Rect.setLength(7);

             Tria.setBase(6);
             Tria.setHeight(8);

             Circle.setRadius(6);

             Square.setLength(7);*/

             Console.WriteLine("Total area [ Rectangle ] : {0}", Rect.getArea());
             Console.WriteLine("Total area [ Triangle  ] : {0}", Tria.getArea());
             Console.WriteLine("Total area [  Circle   ] : {0}", Circle.getArea());
             Console.WriteLine("Total area [  Square   ] : {0}", Square.getArea());
             Console.ReadKey();
        }
    }
}
