﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Shape_Inheritance
{
    class Rectangle : Shape
    {

        protected int width;
        protected int length;
        public void setWidth(int w)
        {
            width = w;
        }

        public void setLength(int l)
        {
            length = l;
        }

        public override double getArea()
        {
            return (length * width);
        }

        public Rectangle(int w, int l)
        {
            width = w;
            length = l;
        }

    }
}
