﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Shape_Inheritance
{
    class Square : Shape
    {
        protected int length;
        public void setLength(int l)
        {
            length = l;
        }

        public override double getArea()
        {
            return (length * length);
        }

        public Square(int l)
        {
            length = l;
        }
    }
}
