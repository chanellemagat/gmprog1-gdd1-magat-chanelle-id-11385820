﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Shape_Inheritance
{
    class Triangle : Shape
    {
        protected int Base;
        protected int height;
        public void setBase(int b)
        {
            Base = b;
        }

        public void setHeight(int h)
        {
            height = h;
        }

        public override double getArea()
        {
            return ((Base * height) / 2);
        }
        public Triangle(int h, int b)
        {
            Base = b;
            height = h;
        }
    }
}
