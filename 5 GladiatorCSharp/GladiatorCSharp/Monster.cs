﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GladiatorCSharp
{
    class Monster : Unit
    {
        // A variable that cannot be modified outside the class
        // But can be accessed outside as read only
        //public string Name { get; private set;}
        //public int hp;
        //public int minAtk;
        //public int maxAtk;

        // Variable w/ only a getter
        // Value cannot be modified anywhere
        public bool IsDead { get { return (hp <= 0); } }

        public Monster(string name, int hp, int minAtk, int maxAtk) : base(name, hp, minAtk, maxAtk)
        {
            // The syntax this.variable assigns the object's internal variable (in this case, the private string name)
            // to be equal to the paramaeter value (name). this.name = name is used to avoid typing name=name w/c can lead
            // to unwanted errors
        }

        //public int Attack(Player target)
        //{
        //    int damage = new Random().Next(minAtk, maxAtk + 1);
        //    target.TakeDamage(damage);
        //    return damage;
        //}

       

        public void PrintStats()
        {
            Console.WriteLine("Name: {0}", Name);
            Console.WriteLine("HP: {0}", hp);
            Console.WriteLine("ATK: {0}-{1}", minAtk, maxAtk);
        }

        //public void PrintHealth()
        //{
        //    Console.WriteLine("{0}'s Current HP: {1}", Name, hp);
        //}
    }
}
