﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GladiatorCSharp
{
    class Player : Unit
    {
        
        public int sp;
        public int armor;

        // Variable w/ only a getter
        // Value cannot be modified anywhere
        List<Skill> Heal = new List<Skill>();
        public bool IsDead { get { return (hp <= 0); } }

        public Player(string name, int hp, int sp, int armor, int minAtk, int maxAtk) : base(name, hp, minAtk, maxAtk)
        {
            // The syntax this.variable assigns the object's internal variable (in this case, the private string name)
            // to be equal to the paramaeter value (name). this.name = name is used to avoid typing name=name w/c can lead
            // to unwanted errors
            this.sp = sp;  
            this.armor = armor;
        }


        // A psuedo-skill that heals for a random amount
        public int Heal()
        {
            // Heal of 10 - 30
            int healAmount = 0;
            if (sp >= 3)
            {
                healAmount = new Random().Next(10, 31);
                hp += healAmount;
            }
            return healAmount;
        }

        public int Bash(Monster target)
        {
            int damage = 0;
            if (sp >= 5)
            {
                damage = new Random().Next(minAtk, maxAtk + 1);
                damage = Convert.ToInt32(damage * 1.3);
                target.TakeDamage(damage);
            }
            return damage;
        }


        public void PrintStats()
        {
            Console.WriteLine("Name: {0}", Name);
            Console.WriteLine("HP: {0}", hp);
            Console.WriteLine("SP: {0}", sp);
            Console.WriteLine("Armor: {0}", armor);
            Console.WriteLine("ATK: {0}-{1}", minAtk, maxAtk);
        }

        //public void PrintHealth()
        //{
        //    Console.WriteLine("{0}'s Current HP: {1}", name, hp);
        //}
    }
}
