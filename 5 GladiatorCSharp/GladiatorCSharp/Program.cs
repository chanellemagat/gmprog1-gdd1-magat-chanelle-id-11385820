﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GladiatorCSharp
{
    class Program
    {
        static Monster GetMonster(int number)
        {
            if (number == 1) return new Monster("Squirell", 10, 1, 5);
            else if (number == 2) return new Monster("Deer", 20, 3, 8);
            else if (number == 3) return new Monster("Wolf", 30, 8, 10);
            else if (number == 4) return new Monster("Lion", 50, 15, 30);
            else if (number == 5) return new Monster("Centaur", 100, 20, 40);
            else if (number == 6) return new Monster("Cerberus", 999, 100, 300);
            else return null;
        }

        static void Main(string[] args)
        {
            Random random = new Random();
            Console.WriteLine("Welcome to the Arena!");
            Console.Write("Please enter your name: ");
            string name = Console.ReadLine();

            // Create the player
            Player player = new Player(name, random.Next(400, 601), random.Next(10, 31), random.Next(1, 6), random.Next(5, 11), random.Next(11, 21));
            player.PrintStats();

            // Start the rounds from 1
            int round = 1;
            Monster currentMonster = null;
            // Game ends when the player dies or kills all monsters
            do
            {
                // Get the next monster
                currentMonster = GetMonster(round);
                Console.WriteLine("You encounter a new monster!");
                currentMonster.PrintStats();
                while (!currentMonster.IsDead && !player.IsDead)
                {
                    // Player actions
                    player.PrintHealth();
                    currentMonster.PrintHealth();
                    Console.WriteLine();
                    int choice = 0;
                    while (choice != 1 && choice != 2 && choice != 3)
                    {
                        Console.Write("Select an action: 1) Attack 2) Heal 3) Bash: ");
                        choice = Convert.ToInt32(Console.ReadLine());
                    }
                    
                    switch (choice)
                    {
                        case 1:
                            {
                                int damageDealt = player.Attack(currentMonster);
                                Console.WriteLine("You deal {0} damage!", damageDealt);
                            }
                            break;
                        case 2:
                            {
                                int healAmount = player.Heal();
                                if (healAmount == 0)
                                {
                                    Console.WriteLine("Cannot heal! No SP!");
                                }
                                else
                                {
                                    Console.WriteLine("You heal yourself for {0} HP!", healAmount);
                                }
                            }
                            break;
                        case 3:
                            {
                                int damageDealt = player.Bash(currentMonster);
                                if (damageDealt == 0)
                                {
                                    Console.WriteLine("Cannot use bash! No SP!");
                                }
                                else
                                {
                                    Console.WriteLine("You deal {0} damage!", damageDealt);
                                }
                                
                            }
                            break;
                    }

                    // Monster actions
                    int monsterDamage = currentMonster.Attack(player);
                    Console.WriteLine("Monster {0} hits you for {1} damage!", currentMonster.Name, monsterDamage);
                }
                if (currentMonster.IsDead)
                {
                    Console.WriteLine("You have killed {0}!", currentMonster.Name);
                }
                Console.ReadKey();
                Console.Clear();
                round++;
            } while (currentMonster != null && !player.IsDead);

        }
    }
}
