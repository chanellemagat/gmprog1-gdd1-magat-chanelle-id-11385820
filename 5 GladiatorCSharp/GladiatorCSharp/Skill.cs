﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GladiatorCSharp
{
    class Skill 
    {
        public string Name;
        //public int sp;
        public int SPCost;
        public Unit caster;

        public Skill(string name, int sp, Unit caster)
        {
            this.Name = name;
            this.SPCost = sp;
            this.caster = caster;
        }

        public virtual double Skill()
        {
            return 0;
        }
    }
}
