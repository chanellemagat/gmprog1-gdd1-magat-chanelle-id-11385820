﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GladiatorCSharp
{
    class Unit
    {
        public string Name;
        public int hp;
        public int minAtk;
        public int maxAtk;

        public Unit(string name, int hp, int minAtk, int maxAtk) 
        {
            this.Name = name;
            this.hp = hp;
            this.minAtk = minAtk;
            this.maxAtk = maxAtk;

        }

        public void TakeDamage(int damage)
        {
            // Clamp the damage value to only reach until 0 HP
            if (hp - damage <= 0)
            {
                hp = 0;
            }
            else
            {
                hp -= damage;
            }
        }

        public int Attack(Unit target)
        {
            int damage = new Random().Next(minAtk, maxAtk + 1);
            target.TakeDamage(damage);
            return damage;
        }

        public void PrintHealth()
        {
            Console.WriteLine("{0}'s Current HP: {1}", Name, hp);
        }

    }
}
