﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CBABasicFramework
{
    public class Component
    {
        public bool Enabled { get; set; }
        public bool HasStarted { get; private set; }
        public bool HasAwoken { get; private set; }
        public GameObject GameObject { get; set; }

        public T GetComponent<T>() where T : Component
        {
            return GameObject.GetComponent<T>();
        }

        public void Awake()
        {
            if (HasStarted) throw new CBAException("Cannot start a Component that has already started");
            HasAwoken = true;

            OnAwake();
        }

        public void Start()
        {
            if (HasStarted) throw new CBAException("Cannot start a Component that has already started");
            HasStarted = true;

            OnStart();
        }

        public void Destroy()
        {
            if (!HasStarted) throw new CBAException("Cannot destroy an unitialized component");
            GameObject.RemoveComponent(this);
        }

        protected virtual void OnStart() { }
        protected virtual void OnAwake() { }

        public virtual void Update() { }
        public virtual void OnDestroy()
        {
            HasAwoken = false;
            HasStarted = false;
            GameObject = null;
        }
    }
}
