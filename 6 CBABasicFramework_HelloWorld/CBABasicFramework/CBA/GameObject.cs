﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CBABasicFramework
{
    public class GameObject
    {
        public string InstanceId { get; private set; }
        public Scene Scene { get; private set; }
        public string Tag { get; set; }
        public bool Enabled { get; set; }
        public bool HasStarted { get; private set; }
        public bool HasAwoken { get; private set; }
        public bool Destroying { get; private set; }

        private List<Component> componentsToAdd = new List<Component>();
        private List<Component> componentsToDestroy = new List<Component>();

        private List<Component> components = new List<Component>();
        public IEnumerable<Component> Components { get { return components; } }

        public GameObject(Scene scene, string instanceId)
        {
            if (scene == null) throw new CBAException("Cannot create a GameObject with null Scene");
            if (string.IsNullOrEmpty(instanceId)) throw new CBAException("Cannot create a GameObject with invalid instance id");

            Scene = scene;
            InstanceId = instanceId;

            Enabled = true;

            Awake();
        }

        #region Components
        public void AddComponent(Component component)
        {
            if (component.GameObject != null) throw new CBAException("Cannot add a component that is already owned by another gameobject");
            if (component.HasAwoken || component.HasStarted) throw new CBAException("Cannot add an already initialized component");
            if (Destroying) return;

            componentsToAdd.Add(component);
        }

        public T AddComponent<T>() where T : Component, new()
        {
            if (Destroying) return null;

            T component = new T();
            AddComponent(component);

            return component;
        }

        public void RemoveComponent<T>() where T : Component
        {
            T component = GetComponent<T>();
            if (component == null) return;

            componentsToDestroy.Add(component);
        }

        public void RemoveComponent(Component component)
        {
            if (!components.Contains(component)) return;

            componentsToDestroy.Add(component);
        }

        public T GetComponent<T>() where T : Component
        {
            Component match = components.Where(c => c is T).FirstOrDefault();
            if (match == null) return null;

            return (T)match;
        }

        public IList<T> GetComponents<T>() where T : Component
        {
            Component[] matches = components.Where(c => c.GetType() is T).ToArray();

            List<T> casted = new List<T>();
            foreach (Component c in matches)
            {
                casted.Add((T)c);
            }

            return casted;
        }
        #endregion

        #region Lifecycle
        public void Start()
        {
            if (HasStarted) throw new CBAException("Cannot start an already started gameobject");
            if (!HasAwoken) throw new CBAException("Cannot start a gameobject that hasn't been awoken yet");
            HasStarted = true;

            foreach (Component c in components)
            {
                c.Start();
            }

            Console.WriteLine("GameObject created: " + InstanceId);
        }

        public void Awake()
        {
            if (HasAwoken) throw new CBAException("Cannot wake up a gameobject that has already been awoken");
            HasAwoken = true;

            foreach (Component c in components)
            {
                c.Awake();
            }
        }

        public void Destroy()
        {
            if (!HasStarted) throw new CBAException("Cannot destroy an uninitialized gameobject");

            Destroying = true;
            foreach (Component c in components)
            {
                c.Destroy();
            }

            Scene.DestroyObject(this);

            Console.WriteLine("GameObject destroyed: " + InstanceId);
        }

        public void Update()
        {
            if (!Enabled) return;

            // Try to wake up the object
            if (!HasAwoken)
            {
                Awake();
            }

            // Try to start the object
            if (!HasStarted)
            {
                Start();
                return;
            }

            // Remove components
            foreach (Component component in componentsToDestroy)
            {
                component.Enabled = false;
                component.GameObject = null;
                components.Remove(component);
                component.OnDestroy();
            }
            componentsToDestroy.Clear();

            // Add components
            foreach (Component component in componentsToAdd)
            {
                component.Enabled = true;
                component.GameObject = this;
                components.Add(component);
                if (HasAwoken) component.Awake();
            }
            componentsToAdd.Clear();

            // Update components
            foreach (Component c in components)
            {
                if (!c.HasStarted)
                {
                    c.Start();
                    continue;
                }
                if (c.HasAwoken && c.HasStarted && c.Enabled) c.Update();
            }
        }

        #endregion
    }
}
