﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CBABasicFramework
{
    public class Scene
    {
        private static Scene currentScene;
        private static readonly float MAX_FPS = 300.0f;
        private float timer = 0;

        private DateTime lastUpdate;

        private List<GameObject> objectsToAdd = new List<GameObject>();
        private List<GameObject> objectsToDestroy = new List<GameObject>();
        private List<GameObject> objects = new List<GameObject>();
        public IEnumerable<GameObject> Objects { get { return objects; } }

        public float DeltaTime { get; private set; }
        public float TimeScale { get; private set; }
        public bool HasStarted { get; private set; }

        
        public static Scene Current
        {
            get
            {
                return currentScene;
            }
            set
            {
                if (currentScene != null)
                {
                    currentScene.Destroy();
                }

                currentScene = value;
                //currentScene.Start();
            }
        }

        public void Start()
        {
            if (HasStarted) throw new CBAException("Cannot start an already started scene");
            if (this != currentScene) throw new CBAException("Scene must be set as current before it can be started");

            HasStarted = true;
            lastUpdate = DateTime.Now;
            Tick();
        }

        void Tick()
        {
            while (HasStarted)
            {
                // Compute delta time
                DateTime now = DateTime.Now;
                long ticks = now.Ticks - lastUpdate.Ticks;
                TimeSpan span = new TimeSpan(ticks);
                float dt = (float)span.TotalSeconds;
                timer += dt;
                lastUpdate = now;

                if (timer < 1.0f / MAX_FPS)
                {
                    continue;
                }

                DeltaTime = timer;

                // Remove objects
                foreach (GameObject g in objectsToDestroy)
                {
                    g.Update();
                    objects.Remove(g);
                }
                objectsToDestroy.Clear();

                // Add objects
                foreach (GameObject g in objectsToAdd)
                {
                    objects.Add(g);
                }
                objectsToAdd.Clear();

                // Update objects
                foreach (GameObject g in objects)
                {
                    g.Update();
                }

                timer = 0;
            }
        }

        public void Destroy()
        {
            HasStarted = false;
            foreach (GameObject g in objects)
            {
                g.Destroy();
            }
        }

        public GameObject CreateObject()
        {
            string guid = Guid.NewGuid().ToString();
            GameObject go = new GameObject(this, guid);
            objectsToAdd.Add(go);

            return go;
        }

        public void DestroyObject(GameObject go)
        {
            objectsToDestroy.Add(go);
        }
    }

    public class CBAException : Exception
    {
        public CBAException(string message)
            : base(message)
        {
        }
    }
}
