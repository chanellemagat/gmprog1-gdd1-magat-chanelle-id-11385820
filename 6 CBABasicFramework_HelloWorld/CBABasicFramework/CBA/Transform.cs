﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CBABasicFramework
{
    public class Transform : Component
    {
        public Vector3 Position = new Vector3();
        public Vector3 Scale = new Vector3();
        public Vector3 Rotation = new Vector3();
    }

    public struct Vector3
    {
        public float x, y, z;
    }

   
}
