﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CBABasicFramework
{
    
    //Inside DirectionalMover script
    //Modify the added Transform component to add x values every second (movement)

    class DirectionalMover : Component
    {
        
        public override void Update()
        {
            base.Update();
            Transform t = GetComponent<Transform>();
            t.Position.x += 1 * Scene.Current.DeltaTime;

            //Console.WriteLine(t.Position.x);
            if (t.Position.x > 1)
            {
                Console.WriteLine(t.Position.x);
            }
            
        }

        

 
    }
}
