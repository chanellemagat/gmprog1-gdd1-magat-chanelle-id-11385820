﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CBABasicFramework
{
    class Program
    {
        static void Main(string[] args)
        {
            Scene.Current = new Scene();
            GameObject go = Scene.Current.CreateObject();
            go.AddComponent<Transform>(); //ADDED
            //go.AddComponent<TestPrint>();
            go.AddComponent<DirectionalMover>();
            Scene.Current.Start();
        }

    }
}
