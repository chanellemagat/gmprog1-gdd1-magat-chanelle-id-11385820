﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CBABasicFramework
{
    public class TestMovement : Component
    {
        private float timer;

        protected override void OnAwake()
        {
            base.OnAwake();

            Console.WriteLine("Awake");
        }

        protected override void OnStart()
        {
            base.OnStart();

            Console.WriteLine("Start");
        }

        public override void OnDestroy()
        {
            base.OnDestroy();

            Console.WriteLine("Destroy");
        }

        public override void Update()
        {
            base.Update();

            if (timer < 3.0f)
            {
                timer += Scene.Current.DeltaTime;
            }
            else
            {
                timer = 0;
                GameObject.Destroy();

                GameObject go = Scene.Current.CreateObject();
                go.AddComponent<TestMovement>();
            }
        }
    }
}
