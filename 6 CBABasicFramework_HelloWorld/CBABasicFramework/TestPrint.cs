﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CBABasicFramework
{
    class TestPrint : Component
    {
        private float timeElapsed = 0.0f;

        public override void Update()
        {
            base.Update();

            timeElapsed += Scene.Current.DeltaTime;
            if (timeElapsed >= 3.0f)
            {
                Console.WriteLine("Hello World");
                timeElapsed = 0.0f;
            }
        }
    }
}
