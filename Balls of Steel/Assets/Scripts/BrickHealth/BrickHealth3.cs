﻿using UnityEngine;
using System.Collections;

public class BrickHealth3 : MonoBehaviour {

	public int Health; 
	// Use this for initialization
	void Start () {
		Health = 3;
	
	}
	
	// Update is called once per frame
	void Update () {
		if(Health == 0)
		{
			Destroy (this.gameObject);

		}
	}
	void OnCollisionEnter (Collision other)
	{
		Health -= 1;
		BrickSpawner.Score = BrickSpawner.Score + 10;
		
	}

	// OnCollisionEnter
	// subtract health by 1
	// if health == 0 destroy
}
