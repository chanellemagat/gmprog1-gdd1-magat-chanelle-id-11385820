﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BrickSpawner : MonoBehaviour {
	public List<Transform> SpawnPoints;
	public List<GameObject> Bricks;
	public GameObject Bomb;
	public static int Score;


	// Use this for initialization
	void Start () {
		// InvokeRepeating -> Calls a method repeatedly over x seconds UnityManual
		SpawnPoints.Capacity = 15;
		Spawn ();
		Instantiate(Bomb, SpawnPoints[Random.Range(0, 15)].position, Quaternion.identity);	 
		Score = 0;

	}
	
	// Update is called once per frame
	void Update () {
		Debug.Log (Score);
	
	}

	void Spawn() {
		for (int i = 0; i < SpawnPoints.Count; i++) {
			Vector3 currentPos = SpawnPoints[i].position;

			Instantiate(Bricks[Random.Range(0, 3)], currentPos, Quaternion.identity);
				}

			}

}

