﻿using UnityEngine;
using System.Collections;

public class Paddle : MonoBehaviour {

	private Rigidbody rb;
	// Use this for initialization
	void Start () {
		// Get rigidbody component here
		rb = GetComponent<Rigidbody> ();
	}
	
	// Update is called once per frame
	void Update () {
		// If A is pressed, add velocity to the left
		// Else if D is pressed, add velocity to the right
		if (Input.GetKey(KeyCode.A))
		{
			this.transform.Translate(0f, 0.5f, 0f);

		}

		if (Input.GetKey(KeyCode.D))
		{
			this.transform.Translate(0f, -0.5f, 0f);;
			
		}
		transform.position = new Vector3 ((Mathf.Clamp (transform.position.x, -3.7f, 3.7f)), transform.position.y, transform.position.z);//(Time.time, -3.53, 3.3));

	}
}
