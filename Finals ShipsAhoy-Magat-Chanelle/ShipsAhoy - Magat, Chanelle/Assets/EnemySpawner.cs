﻿using UnityEngine;
using System.Collections;

public class EnemySpawner : MonoBehaviour {

	public GameObject enemyPrefab1;
	public GameObject enemyPrefab2;
	public GameObject[] gos;

	float spawnDistance = 12f;

	float enemyRate = 5;
	float nextEnemy = 1;

	public void OnCollisionEnter(Collision collision) {
		if (collision.gameObject.tag == "EnemyShip") {
			Physics.IgnoreCollision(collision.collider, GetComponent<Collider>());
		}
	}

	void Awake() {

		Vector3 offset = Random.onUnitSphere;
		
		offset.z = 0;
		
		offset = offset.normalized * spawnDistance;

		gos = new GameObject[2];
		for (int i = 0; i < gos.Length; i++) {

			GameObject clone = (GameObject)Instantiate(enemyPrefab1, transform.position + offset, Quaternion.identity);
			//GameObject clone = (GameObject)Instantiate(enemyPrefab2, transform.position + offset, Quaternion.identity);
			gos[i] = clone;
				}

		}

	// Update is called once per frame
	void Update () {
		nextEnemy -= Time.deltaTime;

		if(nextEnemy <= 0) {
			nextEnemy = enemyRate;
			enemyRate *= 0.9f;
			if(enemyRate < 2)
				enemyRate = 2;

			Vector3 offset = Random.onUnitSphere;

			offset.z = 0;

			offset = offset.normalized * spawnDistance;
			Awake();

			//Instantiate(enemyPrefab1, transform.position + offset, Quaternion.identity);
			Instantiate(enemyPrefab2, transform.position + offset, Quaternion.identity);
		}
	}
}
