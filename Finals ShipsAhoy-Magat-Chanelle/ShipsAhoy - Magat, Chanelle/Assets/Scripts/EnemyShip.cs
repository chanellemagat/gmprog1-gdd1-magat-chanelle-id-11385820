﻿using UnityEngine;
using System.Collections;

public class EnemyShip : MonoBehaviour 
{
	public GameObject Player;
	public float Movespeed = 0.1f;
	public float DistanceToStop;

	private bool anchor;

	void Start() {

		Player = GameObject.FindGameObjectWithTag ("Player");
		anchor = true;
	}

	void Update() {

		if (anchor) {
			transform.position = Vector3.MoveTowards (transform.position, Player.transform.position, Movespeed);
		}

		float Distance = Vector3.Distance (transform.position, Player.transform.position);

		if (Distance <= DistanceToStop) {
			anchor = false;
		} else
			anchor = true;

	}
}
