﻿using UnityEngine;
using System.Collections;


public class MoveForward : MonoBehaviour {

	//public GameObject[] targetEnemy;
	private Vector3 targetPos;
	public float maxSpeed = 5f;

	//private bool engaged;
	//private bool attacking;

	GameObject FindClosestEnemy() 
	{
		GameObject[] gos;
		gos = GameObject.FindGameObjectsWithTag ("Enemy");
		GameObject closest = null;
		float distance = Mathf.Infinity;
		Vector3 position = transform.position;
		foreach (GameObject go in gos) {
			Vector3 diff = go.transform.position - position;
			float curDistance = diff.sqrMagnitude;
			if (curDistance < distance) {
				closest = go;
				distance = curDistance;
			}
			targetPos = closest.transform.position;
		}
		return closest;

	}

	void Start() 
	{
		FindClosestEnemy();
	
	}

	// Update is called once per frame
	void Update () {
		/*float distance = transform.position.z - Camera.main.transform.position.z;
		targetPos = new Vector3 (Input.mousePosition.x, Input.mousePosition.y, distance);
		targetPos = Camera.main.ScreenToWorldPoint (targetPos);*/

	
		transform.position = Vector3.MoveTowards (transform.position, targetPos, 0.08f);
		//if (targetEnemy == null) 
		//{
		//	engaged = false;
		//	attacking = false;
		//}

		/*Vector3 pos = transform.position;
		
		Vector3 velocity = new Vector3(0, maxSpeed * Time.deltaTime, 0);
		
		pos += transform.rotation * velocity;

		transform.position = pos;*/



	}


}
