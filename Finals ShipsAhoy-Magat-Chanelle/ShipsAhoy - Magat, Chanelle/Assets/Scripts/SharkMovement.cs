﻿using UnityEngine;
using System.Collections;

public class SharkMovement : MonoBehaviour {

	public GameObject Player;
	public float Movespeed = 0.1f;
	private Vector3 targetPos;
	private bool anchor;

	// Use this for initialization
	void Start () {

		Player = GameObject.FindGameObjectWithTag ("Player");
		targetPos = Player.transform.position;
			
	}
	
	// Update is called once per frame
	void Update () {

		transform.position = Vector3.MoveTowards (transform.position, targetPos, 0.08f);

	}
}
