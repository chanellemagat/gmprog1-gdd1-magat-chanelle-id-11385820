﻿using UnityEngine;
using System.Collections;

public class ShipMovement : MonoBehaviour 
{
	public GameObject targetEnemy;
	public GameObject bulletPrefab;
	public float bulletSpeed = 100f;

	private Vector3 targetPos;
	private bool engaged;
	private bool attacking;

	void Update()
	{
		float distance = transform.position.z - Camera.main.transform.position.z;
		targetPos = new Vector3 (Input.mousePosition.x, Input.mousePosition.y, distance);
		targetPos = Camera.main.ScreenToWorldPoint (targetPos);

		transform.position = Vector3.MoveTowards (transform.position, targetPos, 0.1f);

		if (targetEnemy == null) 
		{
			engaged = false;
			attacking = false;
		}

		if (engaged && !attacking)
		{
			//StartCoroutine(ShootCountdown(1f));
		}
	}

	void OnTriggerEnter2D(Collider2D other)
	{
		if(other.gameObject.tag == "Enemy")
		{
			targetEnemy = other.gameObject;
			engaged = true;
		}
	}
	/*
	IEnumerator ShootCountdown(float reloadTime)
	{
		attacking = true;
		yield return new WaitForSeconds(reloadTime);
		Shoot ();
		attacking = false;
	}

	void Shoot()
	{
		print ("Shooting!");
		GameObject bullet = (GameObject)Instantiate (bulletPrefab, this.transform.position, this.transform.rotation);
		bullet.AddComponent<Rigidbody2D> ();
		bullet.GetComponent<Rigidbody2D> ().gravityScale = 0;

		//targetPos.Normalize ();
		Vector3 TargetPosition = (targetPos - this.transform.position).normalized;
		bullet.GetComponent<Rigidbody2D> ().AddForce(TargetPosition * bulletSpeed);

		Destroy (bullet, 3f);
	}*/
}
