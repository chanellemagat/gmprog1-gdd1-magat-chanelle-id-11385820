﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RecettearShop
{
    class Item
    {
        public string Name { get; private set; }
        public int BasePrice { get; private set; }

        public Item(string itemName, int basePrice)
        {
            Name = itemName;
            BasePrice = basePrice;
        }

        public void Print()
        {
            Console.WriteLine("{0}: Price:{1}", Name, BasePrice);
        }
    }
}
