﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RecettearShop
{
    class Program
    {
        static void Main(string[] args)
        {
            Random random = new Random();
            Console.WriteLine("Welcome to Recettear!\n");

            Player player = new Player("Recet", 100);
            Shop shop = new Shop(player);

            string input = "";
            while (input != "n")
            {
                int startMoney = player.Pix;
                // Step 1: Setup Shop.
                shop.PrintItems();
                Console.WriteLine("\nSetup Shop");
                string shopInput = "";
                while (true)
                {
                    Console.Write("Would you like to add more items (y)/(n)?");
                    shopInput = Console.ReadLine();
                    if (shopInput == "n")
                    {
                        break;
                    }

                    // Print all items available
                    for (int i = 1; i <= 6; i++)
                    {
                        Console.Write("{0})", i);
                        ItemDatabase.GetItem(i).Print();
                    }
                    // Ask user
                    Console.Write("Please enter the item number you would like to add to your store:");
                    int itemId = Int32.Parse(Console.ReadLine());

                    Item item = ItemDatabase.GetItem(itemId);
                    if (item != null)
                    {
                        shop.AvailableItems.Add(item);
                    }
                    
                    Console.WriteLine();
                }

                shop.PrintItems();

                Console.WriteLine();
                Console.WriteLine("Customers have entered the store!");
                // Step 2: Customers come in
                List<NPC> npcs = new List<NPC>();
                int npcCount = random.Next(1, 6);
                for (int i = 0; i <= npcCount; i++)
                {
                    int npcId = random.Next(1, 7);
                    NPC npc = NPCDatabase.GetNpc(npcId);
                    npcs.Add(npc);
                }

                // Step 3: Resolve orders
                foreach (NPC npc in npcs)
                {
                    Console.WriteLine("A customer is shoping for {0}", npc.ItemNameToBuy);
                    shop.Transact(npc);
                    Console.WriteLine();
                }

                // Step 4: End of the day, display player's earnings
                Console.WriteLine("The shop has closed, you have earned {0}", player.Pix - startMoney);
                Console.Write("Continue playing (y)/(n)?: ");
                input = Console.ReadLine();
            }
        }
    }
}
