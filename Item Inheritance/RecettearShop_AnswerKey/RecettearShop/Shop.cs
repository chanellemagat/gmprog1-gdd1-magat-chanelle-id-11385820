﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RecettearShop
{
    class Shop
    {
        public List<Item> AvailableItems { get; private set; }
        private Player player;

        public Shop(Player p)
        {
            AvailableItems = new List<Item>();
            player = p;
        }

        public void Transact(NPC npc)
        {
            for (int i = 0; i < AvailableItems.Count; i++)
            {
                Item item = AvailableItems[i];
                if (item.Name == npc.ItemNameToBuy)
                {
                    player.Pix += item.BasePrice;
                    AvailableItems.Remove(item);
                    Console.WriteLine("Sold {0} for {1} pix!", item.Name, item.BasePrice);
                    return;
                }
            }
            // Note that this will only execute if the loop fails, the return; statement inside
            // the loop will cause this line not to be executed if a customer was able to buy something
            Console.WriteLine("Customer wasn't able to buy anything :(");
        }

        public void PrintItems()
        {
            Console.WriteLine("\nYour current shop items:");
            if (AvailableItems.Count == 0)
            {
                Console.WriteLine("-Empty-");
            }
            foreach (Item item in AvailableItems)
            {
                item.Print();
            }
        }
    }
}
