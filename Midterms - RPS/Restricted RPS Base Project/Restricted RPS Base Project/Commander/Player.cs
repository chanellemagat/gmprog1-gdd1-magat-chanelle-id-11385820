﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestrictedRPS
{
    public class Player : Commander
    {
        /// <summary>
        /// Get player's input. Player can only either Play or Discard.
        /// </summary>
        public override void EvaluateTurn(Commander opponent)
        {
            int playerChoice;

            Console.WriteLine("1) Play \n2) Discard");            

            playerChoice = Convert.ToInt32(Console.ReadLine());

            if (playerChoice == 1)
            {
                Fight(opponent);
            }

            else if (playerChoice == 2)
            {
                Discard();
            }


            //throw new NotImplementedException();
        }

        /// <summary>
        /// Show a list of cards to choose from. If there are 2 cards of the same type (eg. Warrior), only show one of each type.
        /// </summary>
        /// <returns></returns>
        public override Card PlayCard()
        {
            throw new NotImplementedException();
        }
    }
}
