﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RecettearShop
{
    class Program
    {
        static void Main(string[] args)
        {
            Random random = new Random();
            Console.WriteLine("Welcome to Recettear!\n");

            Player player = new Player("Recet", 100);
            Shop shop = new Shop();

            string input = "";
            while (input != "n")
            {
                // Step 1: Setup Shop.
                shop.PrintItems();
                Console.WriteLine("\nSetup Shop");
                string shopInput = "";
                while (true)
                {
                    Console.Write("Would you like to add more items (y)/(n)?");
                    shopInput = Console.ReadLine();
                    if (shopInput == "n")
                    {
                        break;
                    }

                    /// QUIZ ITEM  -  [x]
                    /// Print all available items in the game
                    /// You can get them from ItemDatabase.GetItem(int id)
                    /// There are only 6 valid items, w/ id numbers from 1-6
                    /// Hint: For loop
                    // Print all items available
                    else if (shopInput == "y")
                    {
                        string[] items = { "1) Walnut Bread: Price:  100", "2) Shortsword:   Price:  300", "3) Longsword:    Price: 1200", "4) Armor:        Price:  800", "5) Shortcake:    Price:  300", "6) Candy:        Price:  120" };
                        for (int i = 0; i < items.Length; i++)
                        {
                            Console.WriteLine(items[i]);
                        }
                        //Shop shop = new Shop;
                        //Console.ReadKey();
                    }
                    

                    /// QUIZ ITEM   - [ ]
                    /// Ask the user w/c item number he would like to add
                    // Ask user
                    Console.Write("Please enter the item number you would like to add to your store:");
                    int itemId = Int32.Parse(Console.ReadLine());
                    int id = 0;
                    //string choice = Console.ReadLine();


                    /// QUIZ ITEM  - [ ]
                    /// Get the item from the database by ItemDatabase.GetItem(int id)
                    itemId = ItemDatabase.GetItem(id = 1);

                   

                    /// QUIZ ITEM  - [ ]
                    /// Add the item to the store's list of available items
                    List<Item> itemList = new List<Item>();
                    //itemList.Add(new Item (){ ItemName = "Walnut Bread", itemId = 1 });
                    Console.WriteLine();
                }

                shop.PrintItems();

                Console.WriteLine();
                Console.WriteLine("Customers have entered the store!");
                // Step 2: Customers come in
                List<NPC> npcs = new List<NPC>();
                int npcCount = random.Next(1, 6);
                for (int i = 0; i <= npcCount; i++)
                {
                    /// QUIZ ITEM  - [x]
                    /// Randomize an NPC ID from 1-6
                    /// and add it to the npcs list
                    /// You can get an NPC by calling
                    /// NPCDatabase.GetNPC(int id)
                    NPC currentNPC = NPCDatabase.GetNpc(random.Next(1,6));
                 
                }

                // Step 3: Resolve orders
                foreach (NPC npc in npcs)
                {
                    Console.WriteLine("A customer is shoping for {0}", npc.ItemNameToBuy);
                    shop.Transact(npc);
                    Console.WriteLine();
                }

                // Step 4: End of the day, display player's earnings
                /// QUIZ ITEM  - [ ] 
                /// Compute the player's money earned
                /// Hint: Store the starting money in a variable before you setup shop
                Console.WriteLine("The shop has closed, you have earned ");
                Console.Write("Continue playing (y)/(n)?: ");
                input = Console.ReadLine();
            }
        }
    }
}
