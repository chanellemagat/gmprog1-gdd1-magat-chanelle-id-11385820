// Armor.h
#pragma once

#include "Range.h"
#include <string>

// <!> Quiz item 6 - Store
// We now represent Armor as an "object" by using a struct
// or a class
struct Armor
{
	std::string mName;
	int mArmorRating;
	int mPrice;

	// Inline constructors
	Armor(std::string name, int armor, int price) {
		mName = name;
		mArmorRating = armor;
		mPrice = price;
	}
	Armor() {
		mName = "";
		mArmorRating = 0;
		mPrice = 0;
	}
};