#include "Item.h"
#include "Player.h"

Item::Item()
{
	mName = "";
	mHpHeal = 0;
	mMpHeal = 0;
	mPrice = 0;
}

Item::Item(std::string name, int hpHeal, int mpHeal, int price)
{
	mName = name;
	mHpHeal = hpHeal;
	mMpHeal = mpHeal;
	mPrice = price;
}

void Item::use(Player &p)
{
	p.addHp(mHpHeal);
	p.addMp(mMpHeal);
}

std::string Item::getName()
{
	return mName;
}

int Item::getHpHeal()
{
	return mHpHeal;
}

int Item::getMpHeal()
{
	return mMpHeal;
}

int Item::getPrice()
{
	return mPrice;
}