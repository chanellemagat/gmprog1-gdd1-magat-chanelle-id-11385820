#pragma once

#include <string>
class Player;

// <!> Quiz item 7 - Items
// We represent both hp and mp healing potions
// as one 'item' object as of now
// there is a better and shorter way to do this with inheritance
class Item
{
public:
	Item();
	Item(std::string name, int hpHeal, int mpHeal, int price);
	
	std::string getName();
	int getHpHeal();
	int getMpHeal();
	int getPrice();

	// Instead of the player 'using' the item to his/herself
	// we put the action to the item instead, so the item now 'applies'
	// it's effects to a player
	// this would support having a party of players controlled by the user
	void use(Player &p);
private:
	std::string mName;
	int	mHpHeal;
	int mMpHeal;
	int mPrice;
};
