// Map.cpp

#include "Map.h"
#include <iostream>
#include "Random.h"
using namespace std;

Map::Map()
{
	// Player starts at origin (0, 0)
	mPlayerXPos = 0;
	mPlayerYPos = 0;
}

int  Map::getPlayerXPos()
{
	return mPlayerXPos;
}

int  Map::getPlayerYPos()
{
	return mPlayerYPos;
}


Monster* Map::getRandomMonster()
{
	int roll = Random(1, 200);
	Monster* monster = 0;

	if(roll <= 45 && roll >= 1)
	{
		//Hit rate = (DEX of attacker / AGI of defender) * 100
		//const std::string& name, int hp, int acc, int Int, int mDex, int mAgi, int mVit
		monster = new Monster("Salamander", 100, ((5 / 10) *100), 10, 5, 10, 10); //Fire

		cout << "You encountered an Salamander!" << endl;
		cout << "Prepare for battle!" << endl;
		cout << endl;
	}
	else if(roll >= 46 && roll <= 90)
	{
		monster = new Monster("Golem", 100, ((5 / 10) *100), 10, 5, 10, 10); //Earth

		cout << "You encountered a Golem!" << endl;
		cout << "Prepare for battle!" << endl;
		cout << endl;
	}
	else if(roll >= 91 && roll <= 135)
	{
		monster = new Monster("Harpy", 100, ((5 / 10) *100), 10, 5, 10, 10); //Wind

		cout << "You encountered an Harpy!" << endl;
		cout << "Prepare for battle!" << endl;
		cout << endl;
	}
	else if(roll >= 136 && roll <= 180 )
	{
		monster = new Monster("Serpent", 100, ((5 / 10) *100), 10, 5, 10, 10); //Water

		cout << "You encountered an Serpent!!!" << endl;
		cout << "Prepare for battle!" << endl;
		cout << endl;
	}
	else if(roll >= 181 && roll <=200)
	{
		monster = new Monster("Demigod", 100, ((8 / 10) *100), 15, 7, 10, 15); //DemiGod
		cout << "You encountered a Demigod!!" << endl;
		cout << "Prepare for battle!" << endl;
		cout << endl;
	}

	return monster;
}

Monster* Map::checkRandomEncounter()
{
	int roll = Random(0, 20);

	Monster* monster = 0;

	if( roll <= 5 )
	{
		// No encounter, return a null pointer.
		return 0;
	}
	else 
	{
		monster = getRandomMonster();
	}

	return monster;
}

void Map::printPlayerPos()
{
	cout << "Player Position = (" << mPlayerXPos << ", " 
		<< mPlayerYPos << ")" << endl << endl;
}