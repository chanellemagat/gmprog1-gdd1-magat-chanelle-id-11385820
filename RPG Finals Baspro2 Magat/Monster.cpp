// Monster.cpp

#include <iostream>
#include <string>
#include "Monster.h"
#include "Player.h"
#include "Random.h"
using namespace std;

Monster::Monster(const std::string& name, int hp, int acc, int Int, int mDex, int mAgi, int mVit)
{
	mName      = name;
	mHitPoints = hp;
	mAccuracy  = acc;

}

bool Monster::isDead()
{
	return mHitPoints <= 0;
}

std::string Monster::getName()
{
	return mName;
}


void Monster::attack(Player& player)
{
	cout << "A " << mName << " attacks you " 
		<< endl;

	// Test to see if the monster hit the player.
	if( Random(0, 20) < mAccuracy )
	{
		int totalDamage = 10;
		if( totalDamage <= 0 )
		{
			cout << "The monster's attack failed to "
				<< "penetrate your armor." << endl;
		}
		else
		{
			cout << "You are hit for " << totalDamage 
				<< " damage!" << endl;

			// Subtract from players hitpoints.
			player.takeDamage(totalDamage);
		}
	}
	else
	{
		cout << "The " << mName << " missed!" << endl;
	}
	cout << endl;
}

void Monster::takeDamage(int damage)
{
	mHitPoints -= damage;
}

void Monster::displayHitPoints()
{
	cout << mName << "'s hitpoints = " << mHitPoints << endl;
}
