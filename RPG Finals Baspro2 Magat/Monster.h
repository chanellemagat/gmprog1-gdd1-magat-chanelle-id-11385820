// Monster.h

#ifndef MONSTER_H
#define MONSTER_H

#include <string>

// "Forward class declaration" so that we can use the Player
// class without having defined it yet.  This idea is 
// similar to a function declaration.

class Player;

class Monster
{
public:
	Monster(const std::string& name, int hp, int acc, int Int, int mDex, int mAgi, int mVit);

	bool isDead();
	std::string getName();

	void attack(Player& player);
	void takeDamage(int damage);
	void displayHitPoints();
	int			mHitPoints;
	int			mAgi;
	

private:
	std::string mName;
	int         mAccuracy;

};

#endif //MONSTER_H