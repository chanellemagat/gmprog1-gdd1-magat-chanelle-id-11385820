// Player.cpp
#include <iostream>
#include <iomanip>
#include <math.h>
#include <string>

#include "Player.h"
#include "Random.h"
#include "Monster.h"

using namespace std;

Player::Player()
{
	mName         = "Default";
	mClassName    = "Default";
	mAccuracy     = 0;
	mHitPoints    = 0;
}

bool Player::isDead()
{
	return mHitPoints <= 0;
}

void Player::takeDamage(int damage)
{
	mHitPoints -= damage;
}

void Player::createClass()
{
	cout << "CHARACTER CLASS GENERATION" << endl;
	cout << "==========================" << endl;

	// Input character's name.
	cout << "Enter your character's name: ";
	getline(cin, mName);

	// Character selection.
	cout << "Please select a character class number..."<< endl;	
	cout << "1)Earth 2)Fire 3)Wind 4)Water : ";	

	int characterNum = 1;	
	cin >> characterNum;

	switch( characterNum )
	{
	case 1:  // Earth
		mClassName    = "Earth";
		mAccuracy     = 7;
		mHitPoints    = 20;
		mVit		  = 10;
		mAgi		  = ((10/20)*100);
		mDex		  = 10;
		mInt		  = 20;
		break;
	case 2:  // Fire
		mClassName    = "Fire";
		mAccuracy     = 5;
		mHitPoints    = 10;
		mVit		  = 10;
		mAgi		  = ((10/20)*100);
		mDex		  = 10;
		mInt		  = 20;
		break;
	case 3:  // Wind
		mClassName    = "Wind";
		mAccuracy     = 8;
		mHitPoints    = 15;
		mVit		  = 10;
		mAgi		  = ((10/20)*100);
		mDex		  = 10;
		mInt		  = 20;
		break;
	default: // Water
		mClassName    = "Water";
		mAccuracy     = ((10/20)*100);
		mHitPoints    = 12;
		mVit		  = 10;
		mAgi		  = 20;
		mDex		  = 10;
		mInt		  = 20;
		break;
	}
}


bool Player::attack(Monster& monster)
{
	int selection = 1;
	cout << "1) Attack 2) Run: ";
	cin >> selection;
	cout << endl;

	switch( selection )
	{
	case 1:
		cout << "You attack an " << monster.getName() << endl;

		if(mClassName == "Water")
		{
			if(mName == "Salamander")
			{
				//Damage = (INT of attacker * Elemental Bonus) � VIT of defender
				int totalDamage = (mInt * 2) - mVit; 
				monster.mHitPoints -= totalDamage;

			}
			else if(mName == "Harpy" && mName == "Demigod")
			{
				int totalDamage = (mInt *.5) - mVit;
				monster.mHitPoints -= totalDamage;
			}
			else
			{
				int totalDamage =(mInt * 1) -mVit;
				monster.mHitPoints -= totalDamage;
			}
		}
		else if(mClassName == "Fire")
		{
			if(mName == "Golem")
			{
				
				int totalDamage = (mInt * 2) - mVit; 
				monster.mHitPoints -= totalDamage;

			}
			else if(mName == "Serpent" && mName == "Demigod")
			{
				int totalDamage = (mInt *.5) - mVit;
				monster.mHitPoints -= totalDamage;
			}
			else
			{
				int totalDamage =(mInt * 1) -mVit;
				monster.mHitPoints -= totalDamage;
			}
		}

		if(mClassName == "Wind")
		{
			if(mName == "Serpent")
			{
				
				int totalDamage = (mInt * 2) - mVit; 
				monster.mHitPoints -= totalDamage;

			}
			else if(mName == "Golem" && mName == "Demigod")
			{
				int totalDamage = (mInt *.5) - mVit;
				monster.mHitPoints -= totalDamage;
			}
			else
			{
				int totalDamage =(mInt * 1) -mVit;
				monster.mHitPoints -= totalDamage;
			}
		}

		if(mClassName == "Earth")
		{
			if(mName == "Harpy")
			{
				
				int totalDamage = (mInt * 2) - mVit; 
				monster.mHitPoints -= totalDamage;

			}
			else if(mName == "Salamander" && mName == "Demigod")
			{
				int totalDamage = (mInt *.5) - mVit;
				monster.mHitPoints -= totalDamage;
			}
			else
			{
				int totalDamage =(mInt * 1) -mVit;
				monster.mHitPoints -= totalDamage;
			}
		}
		else
		{
			cout << "You miss!" << endl;
		}
		cout << endl;
		break;
	
	case 2:
		// 50 % chance of being able to run.
		int roll = Random(1, 4);

		if( roll == 2 )
		{
			cout << "You run away!" << endl;
			return true;
		}
		else
		{
			cout << "You could not escape!" << endl;
			break;
		}
	}

	return false;
}
 


void Player::viewStats()
{
	cout << "PLAYER STATS" << endl;
	cout << "============" << endl;
	cout << endl;
	cout << "Name            = " << mName         << endl;
	cout << "Class           = " << mClassName    << endl;
	cout << "Accuracy        = " << mAccuracy     << endl;
	cout << "Hitpoints       = " << mHitPoints    << endl;
	cout << "Vitality		 = " << mVit		  << endl;
	cout << "Agility		 = " << mAgi		  << endl;
	cout << "Dexterity		 = " << mDex		  << endl;
	cout << "Intelligence	 = " << mInt		  << endl;

	cout << endl;
	cout << "END PLAYER STATS" << endl;
	cout << "================" << endl;
	cout << endl;
}

void Player::victory(int mHitPoints, const std::string& name)
{
	cout << "You won the battle!" << endl;
	cout << "You earn " << mHitPoints 
		<< " additional 30% HP!" << endl << endl;

	mHitPoints += (mHitPoints * .30);
	if(name == "Demigod")
	{
		cout << "Congratulations! The Game has ended :)" << endl;

	}
	else
	{
		mAgi += 10;
		mDex += 5;
		mInt += 5;
		mVit += 10;
	}
}

void Player::gameover()
{
	cout << "You died in battle..." << endl;
	cout << endl;
	cout << "================================" << endl;
	cout << "GAME OVER!" << endl;
	cout << "================================" << endl;
	cout << "Press 'q' to quit: ";
	char q = 'q';
	cin >> q;
	cout << endl;
}

void Player::displayHitPoints()
{
	cout << mName << "'s hitpoints = " << mHitPoints << endl;
}

