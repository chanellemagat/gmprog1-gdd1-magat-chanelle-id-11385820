// Player.h

#ifndef PLAYER_H
#define PLAYER_H

#include "Weapon.h"
#include "Monster.h"
#include <string>
#include <vector>
#include "Spell.h"
#include "Armor.h"
#include "Item.h"

class Player
{
public:
	// Constructor.
	Player();

	// Methods
	bool isDead();

	void takeDamage(int damage);

	void createClass();

	void chooseRace();

	bool attack(Monster& monster);
	void levelUp();
	void viewStats();
	void victory(int mHitPoints, const std::string& name);
	void gameover();
	void displayHitPoints();
	int         mHitPoints;
	void addHp(int value);

private:
	// Data members.
	std::string mName;
	std::string mClassName;
	int         mAccuracy;
	
	int			mVit;
	int			mAgi;
	int			mDex;
	int			mInt;

};

#endif //PLAYER_H