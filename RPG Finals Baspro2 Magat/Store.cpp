#include "Store.h"
#include <iostream>
using namespace std;
Store::Store(void)
{
	// Generate some items
	// The following lines of code are basically calls to the constructors
	// of Weapon, Armor and Item, they are the same as typing:
	//Weapon w1;
	//w1.mName = "Red Rust";
	//w1.mDamageRange.mLow = 1;
	//w1.mDamageRange.mHigh = 4;
	//w1.mPrice = 50;
	//mWeapons.push_back(w1);
	// or another similar version:
	//Weapon w1("Red Rust", 1, 4, 50);
	//mWeapons.push_back(w1);
	mWeapons.push_back(Weapon("Red Rust", 1, 4, 50));
	mWeapons.push_back(Weapon("Long Sword", 3, 8, 100));
	mWeapons.push_back(Weapon("Excalibur", 50, 80, 999));

	mArmors.push_back(Armor("Leather Armor", 5, 70));
	mArmors.push_back(Armor("Chain Mail", 6, 100));
	mArmors.push_back(Armor("Plate Armor", 7, 200));
	
	mItems.push_back(Item("Potion",15,0,10));
	mItems.push_back(Item("Ether",0,15,10));
	mItems.push_back(Item("Elixir",10,10,30));
}

void Store::enter(Player &player)
{
	cout << "Welcome!" << endl;

	int selection = 0;
	while (selection != 3) {
		cout << "Would you like to buy or sell items?" << endl;
		cout << "1) Buy" << endl;
		cout << "2) Sell" << endl;
		cout << "3) Exit" << endl;
		cin >> selection;
		cout << endl;
		if (selection == 1) {
			buy(player);
		} else if (selection == 2) {
			sell(player);
		}
	}
}

void Store::displayInventory(int type)
{
	// Note: the following code can be shortened by a big margin
	// with the use of Inheritance, however at this point
	// it has not been discussed yet
	if (type == 1) {
		for (int i = 0; i < mWeapons.size(); i++) {
			Weapon wep = mWeapons[i];
			cout << i+1 << ")Weapon: " << wep.mName 
				<< " | Damage: " << wep.mDamageRange.mLow << "-" << wep.mDamageRange.mHigh
				<< " | Price: " << wep.mPrice
				<< endl;
		}
	} else if (type == 2) {
		for (int i = 0; i < mArmors.size(); i++) {
			Armor arm = mArmors[i];
			cout << i+1 << ")Armor: " << arm.mName
				<< " | Defense: " << arm.mArmorRating
				<< " | Price: " << arm.mPrice
				<< endl;
		}
	} else if (type == 3) {
		for (int i = 0; i < mItems.size(); i++) {
			Item item = mItems[i];
			cout << i+1 << ")Item: " << item.getName()
				<< " | Heals " << item.getHpHeal() << "HP"
				<< " and " << item.getMpHeal() << "MP"
				<< " | Price: " << item.getPrice()
				<< endl;
		}
	} else {
		cout << "You have not entered a valid selection" << endl;
	}
}

void Store::buy(Player &player)
{
	int selection = 1;
	while (selection > 0 && selection < 4) {
		cout << "What would you like to browse?" << endl;
		cout << "1) Weapons" << endl;
		cout << "2) Armors" << endl;
		cout << "3) Items" << endl;
		cout << "4) Exit" << endl;
		cin >> selection;
		if (selection == 1) { 
			buyWeapon(player);
		}
		else if (selection == 2) {
			buyArmor(player);
		}
		else if (selection == 3) {
			buyItem(player);
		}
	}
}

void Store::buyWeapon(Player &player)
{
	bool valid = false;
	while (!valid) {
		displayInventory(1);
		cout << "Please select a weapon to buy: ";
		int selection;
		cin >> selection;
		// Verify selection is in vector
		if (selection-1 > mWeapons.size()) {
			valid = false;
		} else {
			valid = true;
		}

		// Check if the player has enough money
		Weapon w = mWeapons[selection-1];
		if (player.getGold() >= w.mPrice) {
			// Auto sell the player's current weapon
			player.addGold(player.getWeapon().mPrice);
			// Equip the new weapon
			player.equipWeapon(w);
			player.deductGold(w.mPrice);
		} else {
			cout << "You do not have enough gold!" << endl;
		}
	}
}

void Store::buyArmor(Player &player)
{
	bool valid = false;
	while (!valid) {
		displayInventory(2);
		cout << "Please select an armor to buy: ";
		int selection;
		cin >> selection;
		// Verify selection is in vector
		if (selection-1 > mArmors.size()) {
			valid = false;
		} else {
			valid = true;
		}

		// Check if the player has enough money
		Armor armor = mArmors[selection-1];
		if (player.getGold() >= armor.mPrice) {
			// Auto sell the player's current armor
			player.addGold(player.getArmor().mPrice);
			// Equip the new armor
			player.equipArmor(armor);
			player.deductGold(armor.mPrice);
		} else {
			cout << "You do not have enough gold!" << endl;
		}
	}
}

void Store::buyItem(Player &player)
{
	bool valid = false;
	while (!valid) {
		displayInventory(3);
		cout << "Please select an item to buy: ";
		int selection;
		cin >> selection;
		// Verify selection is in vector
		if (selection-1 > mItems.size()) {
			valid = false;
		} else {
			valid = true;
		}

		// Check if the player has enough money
		Item item = mItems[selection-1];
		if (player.getGold() >= item.getPrice()) {
			// Add the item to the player's current items
			player.getItems().push_back(item);
			player.deductGold(item.getPrice());
		} else {
			cout << "You do not have enough gold!" << endl;
		}
	}
}

void Store::sell(Player &player)
{
	vector<Item> &playerItems = player.getItems();
	// Check if the player has anything to sell
	if (playerItems.size() <= 0) {
		cout << "Nothing to sell!" << endl;
		// A blank return in a void function will exit
		// the function and will not run the code below
		// this is a good technique for breaking out of
		// functions once certain conditions have not been met
		return;
	}
	bool valid = false;
	while (!valid) {
		cout << "Please select an item to sell: " << endl;
		// Note that I opted to declare the counter
		// /outside/ the for loop because
		// the last selection will be to exit
		// e.g. if you have 4 items, a user input of 5
		// means he wants to exit
		int i = 0;
		for (i; i < playerItems.size(); i++) {
			Item item = playerItems[i];
			cout << i+1 << ")Item: " << item.getName()
				<< " | Heals " << item.getHpHeal() << "HP"
				<< " and " << item.getMpHeal() << "MP"
				<< " | Price: " << item.getPrice()
				<< endl;
		}
		cout << i+1 << ") Exit" << endl;
		int selection = 0;
		cin >> selection;
		// We need the +1 in the condition in the end because
		// we're using an additional number to allow the player to exit
		if (selection-1 > playerItems.size()+1) {
			valid = false;
		} else {
			valid = true;
			// If the player selected something in his/her inventory
			if (selection-1 < playerItems.size()) {
				// Sell the item
				Item item = playerItems[selection-1];
				player.addGold(item.getPrice());
				// Remove the item from the player's inventory
				playerItems.erase(playerItems.begin() + (selection-1));
			}
		}
	}
}