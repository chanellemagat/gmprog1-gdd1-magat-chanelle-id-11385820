#pragma once
#include "Player.h"
#include "Weapon.h"
#include "Armor.h"
#include "Item.h"
#include <vector>

// <!> Quiz item 6 - Store
// We model the 'store' as an object, hence it is a class.
// As outlined in the instructions, the store has several methods for
// buying, selling, and displaying items weapons and armors
// note that the Store.cpp is quite long (200+ lines) and can be shortened
// by a very big margin if we use inheritance
class Store
{
public:
	Store(void);

	void enter(Player &player);
private:
	void displayInventory(int type);
	void buy(Player &player);
	void buyWeapon(Player &player);
	void buyArmor(Player &player);
	void buyItem(Player &player);
	void sell(Player &player);
	
	std::vector<Weapon> mWeapons;
	std::vector<Armor> mArmors;
	std::vector<Item> mItems;
};