// Weapon.h

#ifndef WEAPON_H
#define WEAPON_H

#include "Range.h"
#include <string>

struct Weapon
{
	std::string mName;
	Range       mDamageRange;

	// <!> Quiz item 6 - Store
	// We add a price variable to the "Weapon" object
	// to be used by the store
	int			mPrice;
	// Add an inline constructor so it's easy to make Weapon objects
	Weapon(std::string name, int lowDmg, int highDmg, int price) {
		mName = name;
		mDamageRange.mLow = lowDmg;
		mDamageRange.mHigh = highDmg;
		mPrice = price;
	}
	// We also need to specify a default constructor
	Weapon() {
		mName = "";
		mDamageRange.mLow = 0;
		mDamageRange.mHigh = 0;
		mPrice = 0;
	};
};

#endif //WEAPON_H