// game.cpp

#include "Map.h"
#include "Player.h"
#include <cstdlib>
#include <ctime>
#include <iostream>
#include "Spell.h"
#include "Random.h"
#include "Store.h"
using namespace std;

bool simulateCombat(Player &mainPlayer, Monster *monster)
{
	// 'monster' not null, run combat simulation.
	if( monster != 0 )
	{
		// Loop until a 'break' statement.
		while( true )
		{
			// Display hitpoints.
			mainPlayer.displayHitPoints();
			
			
			monster->displayHitPoints();
			cout << endl;

			// Player's turn to attack first.
			bool runAway = mainPlayer.attack(*monster);

			if( runAway )
				break;

			if( monster->isDead() )
			{
				mainPlayer.victory(mainPlayer.mHitPoints, monster->getName());

				break;
			}

			monster->attack(mainPlayer);

			if( mainPlayer.isDead() )
			{
				mainPlayer.gameover();
				return true;
			}
		}
	}

	return false;
}

int main()
{
	srand( time(0) );

	Map gameMap;

	Player mainPlayer;

	mainPlayer.createClass();
	bool done = false;
	while( !done )
	{
		


		int selection = 1;
		cout << "1) Combat, 2) View Stats, 3) Quit: ";
		cin >> selection;

		Monster* monster = 0;
		int restMonsterChance = 0;
		switch( selection )
		{
		case 1:
			monster = gameMap.checkRandomEncounter();
			done = simulateCombat(mainPlayer, monster);
			if( monster != 0 ) 
			{
				delete monster;
				monster = 0;
			}
			break;
		
		case 2:
			mainPlayer.viewStats();
			break;
		case 3:
			done = true;
			break;
		}
	}	
}