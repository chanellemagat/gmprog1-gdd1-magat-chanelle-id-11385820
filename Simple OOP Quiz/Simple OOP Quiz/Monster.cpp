#include "Monster.h"
#include "Player.h"
#include <iostream>

Monster::Monster(string name, string element, int hp)
{
	mName = name;
	mElement = element;
	mHp = hp;
}

void Monster::attack(Player *p)
{
	cout << "A " << Monster.mName<< "attacks you!" << endl;

}

void Monster::takeDamage(int damage, string attackerElement) //[x]
{
	if(mName =="Serpent" && attackerElement == "Fire") //SERPENT's WEAKNESS: fire
	{
		mHp -= damage;
	}
	else if(mName =="Harpy" && attackerElement == "Earth") //HARPY'S WEAKNESS: earth
	{
		mHp -= damage;
	}
	else if(mName =="Golem" && attackerElement == "Wind") //GOLEM'S WEAKNESS: wind
	{
		mHp -= damage;
	}
	else if(mName =="Salamander" && attackerElement == "Water") //SALAMANDER'S WEAKNESS: water
	{
		mHp -= damage;
	}
	//otherwise, 50% bonus for damage
	else
	{
		mHp += (1/2*mHp)+damage;
	}
	
}

std::string Monster::getName()
{
	return mName;
}

void Monster::displayStats()
{
	//SIR I am confused with displaying the pointer to another cpp (randomMonster)

	cout << "\n***************" << endl;
	cout << "Monster's Status" << endl;
	cout << "***************" << endl;
	//cout << monster* new Monster << endl;
	//cout << &monster << endl;
	mHp = 0;
}


bool Monster::isDead()
{
	if (mHp >= 0) return false;
	else return true;
}