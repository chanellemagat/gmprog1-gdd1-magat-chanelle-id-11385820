#pragma once
#include <string>
using namespace std;

class Player;

class Monster
{
public:
	Monster(string name, string element, int hp);
	void attack(Player *p);
	void takeDamage(int damage, string attackerElement);
	void displayStats();
	std::string getName();
	bool isDead();
private:
	int mHp;
	string mElement;
	string mName;
};

