#include "Player.h"
#include "Monster.h"
#include <stdlib.h>
#include <iostream>
using namespace std;

Player::Player()
{
	mName = "Default";
	mElement = "Default";
}

void Player::createCharacter()
{
	// Code to let the user select a name and element here

	//CHARACTER NAME INPUT
	cout << "Please type your Character's name:" << endl;
	getline(cin, mName);
	
	cout<<"Please Select Character's Mage Element: (Fire, Earth, Wind, Water) " << endl;
	getline(cin, mElement);
	if(mElement == "Fire")
	{
		mElement = "Fire";
		
	}
	else if(mElement == "Earth")
	{
		mElement = "Earth";
		
	}
	else if(mElement == "Wind")
	{
		mElement = "Wind";
		
	}
	else if(mElement == "Water")
	{
		mElement = "Water";
		
	}


	//switch(numElement)
	//{
	//case 1: //Fire
	//	mElement = "Fire";
	//	break;

	//}
	mHp = 100;
}

void Player::attack(Monster *p)
{
	int choice = 1;

	cout << "1)Attack!" << endl;
	cin >> choice;
	switch(choice)
	{
		case 1: 
			cout << "You attack a " << Monster.mName << endl;
	}
			
			


}

void Player::takeDamage(int damage, string attackerElement) //[x]
{
	if(mElement == "Water" && attackerElement == "Fire") //Water's WEAKNESS: fire
	{
		mHp -= damage;
	}
	else if(mElement =="Wind" && attackerElement == "Earth") //Wind's WEAKNESS: earth
	{
		mHp -= damage;
	}
	else if(mElement =="Earth" && attackerElement == "Wind") //Earth's WEAKNESS: wind
	{
		mHp -= damage;
	}
	else if(mElement =="Fire" && attackerElement == "Water") //Fire's WEAKNESS: water
	{
		mHp -= damage;
	}
	//otherwise, 50% bonus for damage
	else
	{
		mHp += (1/2*mHp)+damage;
	}
}

void Player::displayStats()
{
	cout << "\nName      : " << mName << endl;
	cout << "Element   : " << mElement << endl;
	cout << "HP        : " << mHp << endl;
}

bool Player::isDead()
{
	if (mHp >= 0) return false;
	else return true;
}