#pragma once
#include <string>
using namespace std;

class Monster;

class Player
{
public:
	Player();
	void createCharacter();
	void attack(Monster *p);
	void takeDamage(int damage, string attackerElement);
	void displayStats();

	bool isDead();
private:
	int mHp;
	string mElement;
	string mName;
};
