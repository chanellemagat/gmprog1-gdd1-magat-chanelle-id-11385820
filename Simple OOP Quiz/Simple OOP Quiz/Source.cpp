#include <iostream>
#include "Monster.h"
#include "Player.h"
#include <stdlib.h>
#include <time.h>
using namespace std;

int main()
{
	srand(time(0));

	// Create the player
	Player *player = new Player();
	player->createCharacter();

	cout << "\n****************" << endl;
	cout << "Your Character:" << endl;
	cout << "****************" << endl;
	player->displayStats();

	// Randomize a monster
	Monster *monster = NULL;
	int randomMonster = rand() % 4;
	if (randomMonster == 0) {
		monster = new Monster("Salamander", "Fire", 30);
	} else if (randomMonster == 1) {
		monster = new Monster("Golem", "Earth", 35);
	} else if (randomMonster == 2) {
		monster = new Monster("Harpy", "Wind", 25);
	} else if (randomMonster == 3) {
		monster = new Monster("Serpent", "Water", 20);
	}

	cout << "You encountered a monster!";
	monster->displayStats();
	
	// Battle until either the monster or player dies
	while (!player->isDead() && !monster->isDead())
	{
		player->attack(monster);
		monster->attack(player);
	}

	if (player->isDead()) {
		cout << "You have died!" << endl;
	} else if (monster->isDead()) {
		cout << "You have defeated the monster!" << endl;
	}

	cin.sync();
	cin.ignore();
	return 0;
}