﻿using UnityEngine;
using System.Collections;

public class Health : MonoBehaviour {

	public float MaxHealth;
	public float CurrentHealth;
	public float Regeneration; //USE DELTA TIME

	// Use this for initialization
	void Start () { //Treat as if it was a Constructor!! (INITIALIZER)
		CurrentHealth = 1;
	
	}
	
	// Update is called once per frame
	void Update () {
		//Time.deltaTime; //multiplication of a thing
		//Assuming it's 1 FPS
		//CurrentHealth += 1 ;

		//assuming you want a  limit for health
		if (MaxHealth >= CurrentHealth) {
			CurrentHealth += (Regeneration * Time.deltaTime);
		} 
	
	}
}
